﻿using LMS.Models;

namespace LMS.Services
{
    public interface IAuthService
    {
        /// <summary>
        /// Perform login function, also update lastLogin time if login success
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>User object logged in, or null if wrong password (email exists, but password wrong)</returns>
        /// <exception cref="Exception">Email does not exists</exception>
        User? Login(string username, string password);
    }
}
