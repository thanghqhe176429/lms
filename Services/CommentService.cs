﻿using LMS.Models;

namespace LMS.Services
{
    public class CommentService : ICommentService
    {
        private readonly LMS_PRN211Context _context;

        public CommentService(LMS_PRN211Context context)
        {
            _context = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comment"></param>
        /// <exception cref="ArgumentNullException">provided comment is null</exception>
        public void AddComment(Comment comment)
        {
            try
            {
                if (comment == null)
                {
                    throw new ArgumentNullException("comment", "comment cannot be null");
                }

                _context.Comments.Add(comment);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void DeleteComment(int id)
        {
            throw new NotImplementedException();
        }

        public Comment? GetComment(int id)
        {
            Comment? comment = _context.Comments.FirstOrDefault(c => c.Id == id);
            return comment;
        }
    }
}
