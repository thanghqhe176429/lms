﻿using LMS.Models;

namespace LMS.Services
{
    public class UserService : IUserService
    {
        private readonly LMS_PRN211Context _context;

        public UserService(LMS_PRN211Context context)
        {
            _context = context;
        }

        public void Create(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            // check for email existence
            if (IsUserExists(user.Email))
            {
                throw new Exception("Email already exists in the database");
            }

            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public bool IsUserExists(string email)
        {
            return _context.Users.SingleOrDefault(u => u.Email == email) == null ? false : true;
        }
    }
}
