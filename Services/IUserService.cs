﻿using LMS.Models;

namespace LMS.Services
{
    public interface IUserService
    {
        void Create(User user);
        bool IsUserExists(string email);
    }
}
