﻿using LMS.Models;
using LMS.Utils;

namespace LMS.Services
{
    public class AuthService : IAuthService
    {

        private readonly LMS_PRN211Context _context;

        public AuthService(LMS_PRN211Context context)
        {
            _context = context;
        }

        /// <summary>
        /// Perform login function, also update lastLogin time if login success
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>User object logged in, or null if wrong password (email exists, but password wrong)</returns>
        /// <exception cref="Exception">Email does not exists</exception>
        public User? Login(string username, string password)
        {
            User? user = _context.Users.FirstOrDefault(u => u.Email == username);
            if (user == null)
            {
                throw new Exception("Email does not exist");
            }

            byte[] salt = user.Salt;
            byte[] hashedPassword = user.HashedPassword;

            // hashing user inputed password
            byte[] enterPass = PasswordUtils.HashPassword(password, salt);

            // compare hashed password. If login sucess
            if (enterPass.SequenceEqual(hashedPassword))
            {
                user.LastLogin = DateTime.Now;
                _context.SaveChanges();
                return user;
            }

            // login failed, wrong password
            return null;
        }
    }
}
