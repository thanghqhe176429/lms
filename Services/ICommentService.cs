﻿using LMS.Models;

namespace LMS.Services
{
    public interface ICommentService
    {
        Comment? GetComment(int id);
        void DeleteComment(int id);
        /// <summary>
        /// Add a comment to a book
        /// </summary>
        /// <param name="comment"></param>
        /// <exception cref="ArgumentNullException"></exception>
        void AddComment(Comment comment);
    }
}
