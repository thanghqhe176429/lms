﻿using LMS.Models;
using LMS.ViewModels;

namespace LMS.Services
{
    public interface IBookService
    {
        /// <summary>
        /// Get all books with its author
        /// </summary>
        /// <returns></returns>
        IEnumerable<Book> GetBooks();
        void AddBook(Book book);
        Book? GetBookById(int id);
        Task<BookMetaData?> GetBookMetaData(string title);
        void UpdateBook(Book book);
    }
}
