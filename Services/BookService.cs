﻿using LMS.Models;
using Microsoft.EntityFrameworkCore;
using Google.Apis.Books.v1;
using Google.Apis.Services;
using Google.Apis.Books.v1.Data;
using LMS.ViewModels;

namespace LMS.Services
{
    public class BookService : IBookService
    {
        private readonly LMS_PRN211Context _context;
        private readonly BooksService _googleBook;

        public BookService(LMS_PRN211Context context)
        {
            _context = context;
            // TODO: Put API key to appsettings.json
            _googleBook = new BooksService(new BaseClientService.Initializer
            {
                ApplicationName = "Google Book",
                ApiKey = "AIzaSyC5lQA80mJEZmtttncnjlBn2Wt-Bq-Lv58"
            }); ;
        }

        public void AddBook(Book book)
        {
            if (book == null)
            {
                throw new Exception("Cannot add book, book is null");
            }

            _context.Books.Add(book);
            _context.SaveChanges();
        }

        /// <summary>
        /// Get book by its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Book object, or null if there is no book with provided id</returns>
        public Book? GetBookById(int id)
        {
            return _context.Books.SingleOrDefault(b => b.Id == id);
        }

        /// <summary>
        /// Get book's metadata from Google Book API by its title. If there are multiple books 
        /// match with title, this function returns the first book match it
        /// </summary>
        /// <param name="title">Title of the book</param>
        /// <returns>The first BookMetaData object match with provided title. Or null if there is no match</returns>
        public async Task<BookMetaData?> GetBookMetaData(string title)
        {
            Volumes result = await _googleBook.Volumes.List(title).ExecuteAsync();
            if (result != null && result.Items.Count > 0)
            {
                Volume volume = result.Items[0];
                BookMetaData bookMetaData = new BookMetaData
                {
                    PageCount = volume.VolumeInfo.PageCount,
                    Thumbnail = volume.VolumeInfo.ImageLinks.Thumbnail,
                    PublishedDate = volume.VolumeInfo.PublishedDate,
                    Description = volume.VolumeInfo.Description,
                    MainCategory = volume.VolumeInfo.MainCategory
                };

                return bookMetaData;
            }

            return null;
        }

        /// <summary>
        /// Get all books with its author
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Book> GetBooks()
        {
            return _context.Books.AsNoTracking().Include(b => b.Authors);
        }

        /// <summary>
        /// Update book by is Id
        /// </summary>
        /// <param name="book"></param>
        /// <exception cref="Exception">If bookId does not exist</exception>
        public void UpdateBook(Book book)
        {
            Book? temp = GetBookById(book.Id);
            if(temp == null)
            {
                throw new Exception("Cannot update book, bookId does not exist");
            }

            _context.Entry(temp).State = EntityState.Detached;
            _context.Books.Update(book);
            _context.SaveChanges();
        }
    }
}
