﻿using Microsoft.AspNetCore.Mvc;
using LMS.Services;
using LMS.Models;
using System.Text.Json;

namespace LMS.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        // TODO: Need to be fix later (model binding from view not very clean)
        [HttpPost]
        public IActionResult Add(Comment comment)
        {

            // Check for user authentication
            ISession session = HttpContext.Session;
            string jsonUsr = session.GetString("loggedUser");
            User? user = null;
            if (jsonUsr != null) // user already logged in
            {
                user = JsonSerializer.Deserialize<User>(jsonUsr);
            } else // if user not login, redirect to homepage
            {
                return RedirectToAction("Index", "Home");
            }

            // setup the correct info for comment
            comment.PostTime = DateTime.Now;
            comment.Title = comment.Title;
            comment.UserId = user.Id;

            _commentService.AddComment(comment);
            return RedirectToAction("Index", "Book");
        }
    }
}
