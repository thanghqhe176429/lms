﻿using Microsoft.AspNetCore.Mvc;
using LMS.Services;
using LMS.Models;
using LMS.Utils;
using LMS.ViewModels;
using System.Text.Json;

namespace LMS.Controllers
{
    public class AuthController : Controller
    {

        private readonly IUserService _userService;
        private readonly IAuthService _authService;
        private readonly LMS_PRN211Context _context;

        public AuthController(IUserService userService, IAuthService authService, LMS_PRN211Context context)
        {
            _userService = userService;
            _authService = authService;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register(string name, string email, string password, string rePassword)
        {
            if (password != rePassword)
            {
                // TODO: Create a more appropriate page
                return NotFound();
            }

            byte[] salt = PasswordUtils.GenerateSalt();
            byte[] hashedPassword = PasswordUtils.HashPassword(password, salt);

            User user = new User
            {
                FullName = name,
                Email = email,
                Salt = salt,
                HashedPassword = hashedPassword, 
                RegisteredAt = DateTime.UtcNow,
                LastLogin = DateTime.UtcNow,
                RoleId = 2 // default to role User
            };
            _userService.Create(user);
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel model)
        {
            try
            {
                User? user = _authService.Login(model.Username, model.Password);
                if(user == null)
                {
                    ViewData["ErrorMsg"] = "Wrong email or password";
                    return View();
                }

                ISession session = HttpContext.Session;
                session.SetString("loggedUser", JsonSerializer.Serialize<User>(user));
                return RedirectToAction("Index", "Home");
            }catch(Exception ex)
            {
                ViewData["ErrorMsg"] = ex.Message;
                return View();
            }
        }
    }
}
