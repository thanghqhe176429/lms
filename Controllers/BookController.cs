﻿using Microsoft.AspNetCore.Mvc;
using LMS.Services;
using LMS.Models;
using LMS.ViewModels;

namespace LMS.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }
        public IActionResult Index()
        {
            IEnumerable<Book> books = _bookService.GetBooks();
            return View(books);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create([Bind("Title, Isbn, Edition, Publisher, Description")] Book book)
        {
            // set default status for book
            book.Status = "AVAIL";

            try
            {
                _bookService.AddBook(book);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMsg = ex.Message;
                return View();
            }


        }

        public async Task<IActionResult> Details(int id)
        {
            // get book's info from database
            Book? book = _bookService.GetBookById(id);
            if (book == null)
            {
                ViewData["ErrorMsg"] = "Error 404: Book not found";
                return View();
            }

            // Get book's meta data based on book's title
            BookMetaData? bookMetaData = await _bookService.GetBookMetaData(book.Title);

            BookDetails bookDetails = new BookDetails
            {
                BookModel = book,
                BookMetaData = bookMetaData
            };
            return View(bookDetails);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Book? book = _bookService.GetBookById(id.Value);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        [HttpPost]
        public IActionResult Edit([Bind("Id, Title, Isbn, Edition, Description")] Book editBook, int id)
        {
            if (editBook.Id != id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _bookService.UpdateBook(editBook);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ViewData["ErrorMsg"] = ex.Message;
                    return View(editBook);
                }
            }
            else
            {
                return View(editBook);
            }
        }
    }
}
