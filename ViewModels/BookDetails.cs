﻿using LMS.Models;

namespace LMS.ViewModels
{
    public class BookDetails
    {
        public Book BookModel { get; set; }
        public BookMetaData? BookMetaData { get; set; }
    }
}
