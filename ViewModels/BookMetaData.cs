﻿namespace LMS.ViewModels
{
    /// <summary>
    /// Represent metadata (additional information) about a book. These information 
    /// are get from Google Book API, and not from the database
    /// </summary>
    public class BookMetaData
    {
        public int? PageCount { get; set; }
        public string? Thumbnail { get; set; }
        public string? PublishedDate { get; set; }
        public string? Description { get; set; }
        public string? MainCategory { get; set; }
    }
}
