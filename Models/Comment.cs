﻿using System;
using System.Collections.Generic;

namespace LMS.Models
{
    public partial class Comment
    {
        public Comment()
        {
            InverseParent = new HashSet<Comment>();
        }

        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string Text { get; set; } = null!;
        public DateTime PostTime { get; set; }
        public int? ParentId { get; set; }
        public int BookId { get; set; }
        public int UserId { get; set; }

        public virtual Book Book { get; set; } = null!;
        public virtual Comment? Parent { get; set; }
        public virtual User User { get; set; } = null!;
        public virtual ICollection<Comment> InverseParent { get; set; }
    }
}
