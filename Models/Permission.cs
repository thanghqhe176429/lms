﻿using System;
using System.Collections.Generic;

namespace LMS.Models
{
    public partial class Permission
    {
        public Permission()
        {
            Roles = new HashSet<Role>();
        }

        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public byte Active { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
