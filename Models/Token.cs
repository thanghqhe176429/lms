﻿using System;
using System.Collections.Generic;

namespace LMS.Models
{
    public partial class Token
    {
        public int Id { get; set; }
        public string? Token1 { get; set; }
        public int UserId { get; set; }
        public DateTime GeneratedTime { get; set; }
        public string Status { get; set; } = null!;

        public virtual User User { get; set; } = null!;
    }
}
