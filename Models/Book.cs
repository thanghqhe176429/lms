﻿using System;
using System.Collections.Generic;

namespace LMS.Models
{
    public partial class Book
    {
        public Book()
        {
            BookDetails = new HashSet<BookDetail>();
            BorrowRecords = new HashSet<BorrowRecord>();
            Comments = new HashSet<Comment>();
            Authors = new HashSet<Author>();
        }

        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string? Isbn { get; set; }
        public int? Edition { get; set; }
        public string? Publisher { get; set; }
        public string? Description { get; set; }
        public string? Status { get; set; }

        public virtual ICollection<BookDetail> BookDetails { get; set; }
        public virtual ICollection<BorrowRecord> BorrowRecords { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Author> Authors { get; set; }
    }
}
