﻿using System;
using System.Collections.Generic;

namespace LMS.Models
{
    public partial class BookDetail
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public string? Location { get; set; }
        public int Copies { get; set; }

        public virtual Book Book { get; set; } = null!;
    }
}
