﻿using System;
using System.Collections.Generic;

namespace LMS.Models
{
    public partial class User
    {
        public User()
        {
            BorrowRecordBorrowers = new HashSet<BorrowRecord>();
            BorrowRecordLibrarians = new HashSet<BorrowRecord>();
            Comments = new HashSet<Comment>();
            Tokens = new HashSet<Token>();
        }

        public int Id { get; set; }
        public int RoleId { get; set; }
        public string? FullName { get; set; }
        public string? Mobile { get; set; }
        public string? Email { get; set; }
        public byte[]? Salt { get; set; }
        public byte[]? HashedPassword { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime? LastLogin { get; set; }

        public virtual Role Role { get; set; } = null!;
        public virtual ICollection<BorrowRecord> BorrowRecordBorrowers { get; set; }
        public virtual ICollection<BorrowRecord> BorrowRecordLibrarians { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Token> Tokens { get; set; }
    }
}
