--USE master;
--CREATE DATABASE LMS_PRN211;
--DROP DATABASE LMS_PRN211;
--USE LMS_PRN211;

CREATE TABLE Role(
	id INT NOT NULL IDENTITY(1,1),
	title VARCHAR(50) NOT NULL
);
ALTER TABLE Role ADD CONSTRAINT pk_roleId PRIMARY KEY(id);

CREATE TABLE [User](
	id INT NOT NULL IDENTITY(1,1),
	roleId INT NOT NULL,
	fullName NVARCHAR(50) DEFAULT NULL,
	mobile VARCHAR(15),
	email VARCHAR(50),
	salt VARBINARY(16),
	hashedPassword VARBINARY(32),
	registeredAt DATETIME NOT NULL,
	lastLogin DATETIME DEFAULT NULL,
);
ALTER TABLE [User] ADD CONSTRAINT pk_userId PRIMARY KEY(id);
ALTER TABLE [User] ADD CONSTRAINT fk_role FOREIGN KEY(roleId) REFERENCES Role(id);
ALTER TABLE [User] ADD CONSTRAINT uc_email UNIQUE(email);

CREATE TABLE Permission (
	id INT NOT NULL IDENTITY(1,1),
	title VARCHAR(50) NOT NULL,
	active TINYINT NOT NULL DEFAULT 0,
);
ALTER TABLE Permission ADD CONSTRAINT pk_permId PRIMARY KEY(id);

CREATE TABLE Role_Permission (
	roleId INT NOT NULL,
	permissionId INT NOT NULL,
);
ALTER TABLE Role_Permission ADD CONSTRAINT pk_role_perm PRIMARY KEY(roleId, permissionId);
ALTER TABLE Role_Permission ADD CONSTRAINT fk_roleId FOREIGN KEY(roleId) REFERENCES Role(id);
ALTER TABLE Role_Permission ADD CONSTRAINT fk_permissionId FOREIGN KEY(permissionId) REFERENCES Permission(id);

CREATE TABLE Book (
	id INT NOT NULL IDENTITY(1, 1),
	title NVARCHAR(50) NOT NULL,
	isbn VARCHAR(20),
	edition INT,
	publisher VARCHAR(20),
	description NVARCHAR(200),
	status VARCHAR(5)
);
ALTER TABLE Book ADD CONSTRAINT pk_bookId PRIMARY KEY(id);
--DROP TABLE Book;

CREATE TABLE Author (
	id INT NOT NULL IDENTITY(1,1),
	name NVARCHAR(50)
);
ALTER TABLE Author ADD CONSTRAINT pk_author PRIMARY KEY(id);

CREATE TABLE BookAuthor (
	bookId INT NOT NULL,
	authorId INT NOT NULL
);
ALTER TABLE BookAuthor ADD CONSTRAINT pk_book_author PRIMARY KEY(bookId, authorId);
ALTER TABLE BookAuthor ADD CONSTRAINT fk_book FOREIGN KEY(bookId) REFERENCES Book(id);
ALTER TABLE BookAuthor ADD CONSTRAINT fk_author FOREIGN KEY(authorId) REFERENCES Author(id);

CREATE TABLE Book_Details (
	id INT NOT NULL,
	bookId INT NOT NULL,
	location VARCHAR(50),
	copies INT NOT NULL,
);
ALTER TABLE Book_Details ADD CONSTRAINT pk_detailsId PRIMARY KEY(id);
ALTER TABLE Book_Details ADD CONSTRAINT fk_bookId FOREIGN KEY(bookId) REFERENCES Book(id);

CREATE TABLE BorrowRecord (
	id INT NOT NULL,
	bookId INT NOT NULL,
	borrowerId INT NOT NULL,
	librarianId INT NOT NULL,
	startDate DATETIME NOT NULL,
	endDate DATETIME,
	returnDate DATETIME,
	quantity INT NOT NULL,
	status VARCHAR(5) NOT NULL
);
ALTER TABLE BorrowRecord ADD CONSTRAINT pk_brId PRIMARY KEY(id);
ALTER TABLE BorrowRecord ADD CONSTRAINT fk_borrowerId FOREIGN KEY(borrowerId) REFERENCES [User](id);
ALTER TABLE BorrowRecord ADD CONSTRAINT fk_librarianId FOREIGN KEY(librarianId) REFERENCES [User](id);
ALTER TABLE BorrowRecord ADD CONSTRAINT fk_bookBorrowId FOREIGN KEY(bookId) REFERENCES Book(id);

CREATE TABLE Comment (
	id INT NOT NULL IDENTITY(1,1),
	title NVARCHAR(50) NOT NULL,
	text NVARCHAR(500) NOT NULL,
	postTime DATETIME NOT NULL,
	parentId INT,
	bookId INT NOT NULL,
	userId INT NOT NULL
);
ALTER TABLE Comment ADD CONSTRAINT pk_commentId PRIMARY KEY(id);
ALTER TABLE Comment ADD CONSTRAINT fk_comment FOREIGN KEY(parentId) REFERENCES Comment(id);
ALTER TABLE Comment ADD CONSTRAINT fk_user_comment FOREIGN KEY(userId) REFERENCES [User](id);
ALTER TABLE Comment ADD CONSTRAINT fk_comment_book FOREIGN KEY(bookId) REFERENCES Book(id);

---------------------- INSERT DATA ------------------------
INSERT INTO Role(title) VALUES
('Librarian'), ('Student');