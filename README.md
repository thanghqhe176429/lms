# Library Management System

## Introduction

This project was developed as an upgraded version of an assigment for PRN211 in FPT University

## Tools

- ASP.NET Core MVC
- .NET 6
- EF Core
- Visual Studio 2022
- SQL Server

## Project structure

This project has the following basic folders

- Controllers: Contains all controller classes
- Extensions: Contains helper methods
- Models: Generated classes from reverse engineering EF Core approach
- Service: Contains business logic, these classes interact with DbContext to do logic
- ViewModels: View file used to display information to user
- EditModels: View file used in edit page
- Views: General view files
- Filter: Filter classes, mostly used for authorization

## Features

- CRUD Books
  - Create: Create book
  - Details: View detailed information of a book (from database + meta data from Google Book)
- CRUD Users
- Create borrow book transactions
- Return books

## Plans

- CRUD and search on all pages
- Google Book API
- Sending email (current version can send email using a virtual mail server, but it can adopt to use Google Mail Server instead)
- Security (Authorization for many roles, basic security attacks preventation)
- Calculate fine when return book late
- Redesign of database for partial return of books
- UI Bootstrap Template (currently uses default UI provided by ASP.NET Core)

## Limitation

Due to limited in knowledge, time and member, many CRUDs are not implemented yet

## References and tutorials

[Send email using Mailkit](https://blog.christian-schou.dk/send-emails-with-asp-net-core-with-mailkit/)